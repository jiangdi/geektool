package cn.geektool.http.server;

import cn.geektool.core.swing.DesktopUtil;
import cn.geektool.http.ContentType;
import cn.geektool.http.HttpUtil;

public class BlankServerTest {
	public static void main(String[] args) {
		HttpUtil.createServer(8888)
				.addAction("/", (req, res)-> res.write("Hello Hutool Server", ContentType.JSON.getValue()))
				.start();

		DesktopUtil.browse("http://localhost:8888/");
	}
}
