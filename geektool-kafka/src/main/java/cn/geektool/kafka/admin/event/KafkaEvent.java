package cn.geektool.kafka.admin.event;

/**
 * kafka操作事件
 *
 * @author jiangdi
 * @since 0.0.1
 */
public enum KafkaEvent {
    /**
     * 暂停
     * mut not supported
     */
    PAUSE,
    /**
     * 继续
     * mut not supported
     */
    RESUME,
    /**
     * 销毁
     */
    DESTROY,
    /**
     * 创建
     */
    CREATE
}
