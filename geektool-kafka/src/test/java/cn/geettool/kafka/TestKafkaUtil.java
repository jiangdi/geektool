//package cn.geettool.kafka;
//
//import cn.geektool.core.lang.Console;
//import cn.geektool.kafka.admin.KafkaBuilder;
//import cn.geektool.kafka.admin.KafkaFramework;
//import cn.geektool.kafka.admin.external.IKafkaConsumer;
//import org.apache.kafka.clients.producer.RecordMetadata;
//import org.junit.Test;
//
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.Future;
//
//public class TestKafkaUtil {
//
//
////    // @Test
//    public void TestProd(){
//        //获取kafka组装器
//        KafkaBuilder.Builder builder = KafkaBuilder.builder();
//        //设置使用生产者
//        builder.enableProducer(true);
//        //构建生产者
//        KafkaBuilder.ProducerBuilder producerBuilder = builder.createProducerBuilder();
//        //  生产者指定对应的服务
//
//        producerBuilder.enable(true).servers("49.233.85.65:9092");
//        //绑定生产者到组装器
//        builder.bindProducer("test",producerBuilder.builder());
//        //根据组装器构建到kafka
//        KafkaFramework kafkaFramework = builder.builder();
//        //启动kafka
//        kafkaFramework.start();
//        int i = 0;
//        //用于测试发送
//        while (i<10){
//            Future<RecordMetadata> future =  kafkaFramework.getSuccessProducerMap().get("test").send("topic001","name","1",(meta, exception)->{
//                Console.log(meta);//错误exception才有值
//                Console.log(exception);
//            });
//            try {
//                future.get();
//                Thread.sleep(1000L);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//            i++;
//        }
//    }
//
//    // @Test
//    public void TestConsumer(){
//        //消费调用
//        //获取kafka组装器
//        KafkaBuilder.Builder builder = KafkaBuilder.builder();
//        //开启消费者
//        builder.enableConsumer(true);
//        //获得消费者对应构造器
//        KafkaBuilder.ConsumerBuilder consumerBuilder = builder.createConsumerBuilder();
//        consumerBuilder.enable(true).servers("49.233.85.65:9092").topics("topic001").groupName("IDC-NF-CTSI")
//                .invokeBeanName("cn.geektool.kafka.TestConsumer")
//                .invokeMethodName("test");
//        builder.bindConsumer("test", consumerBuilder.builder());
//        KafkaFramework framework = builder.builder();
//        framework.start();
//
//        IKafkaConsumer consumer = framework.getSuccessConsumerMap().get("IDC-NF-CTSI");
//        try {
//            consumer.run();
//            Thread.sleep(1000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    public static void main(String[] args) {
////        for (int i = 0; i < 5; i++) {
//            Console.log("获取到："+((int)(Math.random()*10)));
////        }
////        int sum=0;
////        float average;
////        Console.log("随机列出十个数字是:");
////        for(int i=0;i<10;i++){
////            int n=(int)(Math.random()*10+1);
////            sum=sum+n;
////            System.out.print(n+" ");
////        }
////        System.out.println();
////        average=sum/10.0f;
////        Console.log("它们的和为:"+sum);
////        Console.log("它们的平均数为"+average);
//    }
//
//
//}
