# geektool


> 我是一个想成为黑客的程序员，一直比较困扰在工作中总是会遇到很多曾经写过的工具，下次使用的时候又要重新开始写或者找资料，于是乎下定决心整合出一个工具。  
> 名为：GeekTool ，意味 “极客工具”，算是对我曾经梦想的一种怀念。
> 极客：极客是美国俚语“geek”的音译。随着互联网文化的兴起，这个词含有智力超群和努力的语意，又被用于形容对计算机和网络技术有狂热兴趣并投入大量时间钻研的人。

GeekTool 提供对应第三方快速接入以及使用框架优化代码写法等操作、并且封装了相关接入、工具使用方式，使 Java 开发变得如函数般优雅。

<table>
    <thead>
        <tr>
            <th>项目名</th>
            <th>说明</th>
            <th>源码</th>
            <th>文档</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>geektool-core</td>
            <td>无第三方依赖的工具包</td>
            <td>
                <a href="https://gitee.com/jiangdi/geektool" target="_blank" rel="noopener">访问</a>
            </td>
            <td>
                <a href="http://doc.qzdvfx.com/project-1/doc-3/" target="_blank" rel="noopener">访问</a>
            </td>
        </tr>
        <tr>
            <td>geektool-kafka</td>
            <td>基于 apache kafka 进行二次封装工具包</td>
            <td>
                <a href="https://gitee.com/jiangdi/geektool/" target="_blank" rel="noopener">访问</a>
            </td>
            <td>
                <a href="http://doc.qzdvfx.com/project-1/doc-1/" target="_blank" rel="noopener">访问</a>
            </td>
        </tr>

</tbody>
</table>
# Maven引用
所有工具子模块集合引用，包含所有工具模块

```
<!-- https://mvnrepository.com/artifact/cn.geektool/geektool-all -->
<dependency>
    <groupId>cn.geektool</groupId>
    <artifactId>geektool-all</artifactId>
    <version>last</version>
</dependency>
```

核心模块引用
```
<!-- https://mvnrepository.com/artifact/cn.geektool/geektool-all -->
<dependency>
    <groupId>cn.geektool</groupId>
    <artifactId>geektool-core</artifactId>
    <version>last</version>
</dependency>
``` 
kafka模块引用
```
<!-- https://mvnrepository.com/artifact/cn.geektool/geektool-all -->
<dependency>
    <groupId>cn.geektool</groupId>
    <artifactId>geektool-kafka</artifactId>
    <version>last</version>
</dependency>
```
