package cn.geektool.core.convert.impl;

import java.nio.charset.Charset;

import cn.geektool.core.util.CharsetUtil;
import cn.geektool.core.convert.AbstractConverter;

/**
 * 编码对象转换器
 * @author jd
 *
 */
public class CharsetConverter extends AbstractConverter<Charset>{
	private static final long serialVersionUID = 1L;

	@Override
	protected Charset convertInternal(Object value) {
		return CharsetUtil.charset(convertToStr(value));
	}

}
