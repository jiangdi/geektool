/**
 * 建造者工具<br>
 * 用于建造特定对象或结果
 * 
 * @author jd
 *
 */
package cn.geektool.core.builder;