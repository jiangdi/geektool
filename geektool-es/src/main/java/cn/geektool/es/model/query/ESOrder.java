package cn.geektool.es.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.geektool.es.server.query.OrderType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ESOrder {

    private String fieldName;

    private OrderType orderType;
}