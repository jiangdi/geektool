package cn.geektool.es;


import cn.geektool.core.lang.Console;
import cn.geektool.es.server.query.ESSearchBuilder;
import cn.geektool.es.server.query.OperatorType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import cn.geektool.es.admin.ESBuilder;
import cn.geektool.es.admin.ESFramework;
import cn.geektool.es.admin.factory.IESFactory;
import cn.geektool.es.server.annoation.ESTable;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * 测试
 *
 * @author jiangdi
 * @since 1.0.0
 */
@Slf4j
public class TestSample {

    public static void main(String[] args) {
        //获取ES构建类
        ESBuilder.Builder builder = ESBuilder.builder();
        //创建ES生产
        ESBuilder.ProducerBuilder producerBuilder = builder.createProducerBuilder();
        //绑定一个zhangsan地址65到framework,可同时绑定多个 默认端口是9200
        ESFramework framework = builder.bindProducer("zhangsan",
                      producerBuilder.ip("49.233.85.65").enable(true).builder()).builder();
        //启动ES平台
        framework.start();
        //根据绑定的key获取对应的工厂

        IESFactory factory = framework.getFactory("zhangsan");
        try {
            //ES新增
//            IndexResponse response = factory.insert("{\"age\":22}","test");
//            log.info("新增1:{}",response);
//            Test test = new Test();
//            IndexResponse response2 = factory.insert(test);
//            log.info("新增2:{}",response2);
//            String id = response2.getId();
//            test.setName("你好");
//            //ES修改
//            UpdateResponse response3 = factory.update(test,id);
//            log.info("修改:{}",response3);
//            //ES删除数据
//            DeleteResponse response4 = factory.delete("test",id);
//            log.info("删除:{}",response4);
//            //ES删除索引及数据
//            AcknowledgedResponse response5 = factory.delete("test");
//            log.info("删除索引:{}",response5);
//            获取RestHighLevelClient对象
            RestHighLevelClient client = factory.getClient();
            log.info("打印信息{}",client.info(RequestOptions.DEFAULT));
            testMultiSearch(client);

//            Console.log(test1);
//            ESSearchBuilder builder1 = factory.getBuilder().addCondition("age", OperatorType.LIKE,"22");
//            log.info("builder{}",builder1);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Data
    @ESTable(index = "test")
    static class Test {
        private String name = "张三";
    }

    public static void testMultiSearch(RestHighLevelClient client){
        MultiSearchRequest request = new MultiSearchRequest();
        SearchRequest firstSearchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("cityId", "410500"));
        firstSearchRequest.source(searchSourceBuilder);
        firstSearchRequest.indices("sub_bank1031");
        request.add(firstSearchRequest);
        SearchRequest secondSearchRequest = new SearchRequest();
        searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("cityId", "511000"));
        secondSearchRequest.source(searchSourceBuilder);
        secondSearchRequest.indices("sub_bank1031");
        request.add(secondSearchRequest);
        try {
            MultiSearchResponse response = client.msearch(request, RequestOptions.DEFAULT);
            response.forEach(t->{
                SearchResponse resp = t.getResponse();

                Arrays.stream(resp.getHits().getHits())
                        .forEach(i -> {
                            System.out.println(i.getId());
                            System.out.println(i.getIndex());
                            System.out.println(i.getSourceAsString());
                            System.out.println(i.getShard());
                        });
                System.out.println(resp.getHits());
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
