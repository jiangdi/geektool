package cn.geektool.core.lang.generator;

import cn.geektool.core.util.IdUtil;

/**
 * UUID生成器
 *
 * @author jd
 * @since 5.4.3
 */
public class UUIDGenerator implements Generator<String> {
	@Override
	public String next() {
		return IdUtil.fastUUID();
	}
}
