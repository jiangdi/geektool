package cn.geektool.core.io.watch;

import cn.geektool.core.io.watch.watchers.IgnoreWatcher;

/**
 * 空白WatchListener<br>
 * 用户继承此类后实现需要监听的方法
 * @author jd
 *
 */
public class SimpleWatcher extends IgnoreWatcher {
	
}
