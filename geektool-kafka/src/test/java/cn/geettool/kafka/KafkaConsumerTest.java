//package cn.geettool.kafka;
//
//
//
//import cn.geektool.core.date.DateUtil;
//import cn.geektool.core.lang.Console;
//import cn.geektool.json.JSONObject;
//import cn.geektool.json.JSONUtil;
//import cn.geektool.kafka.util.KafkaConsumerUtil;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @description:
// * @author: Wang Hongyu
// * @date: 2020-08-19
// */
//public class KafkaConsumerTest {
//    @Test
//    public void test() throws InterruptedException {
//        List<String> stringList = new ArrayList<>();
//        stringList.add("UCCP-CMP-FJM-ALARM");
//        KafkaConsumerUtil kafkaConsumerUtil
//                = new KafkaConsumerUtil("10.135.10.92:19092","UCCP-CMP-POC-S2",null,stringList,"latest");
//        kafkaConsumerUtil.init();
//        while(true){
//            List<String> result = kafkaConsumerUtil.getKafkaData(1);
//            result.parallelStream().forEach(str-> {
//                Console.log("===================消息来啦=====================");
//                Console.log(str);
//                String data = str.substring(str.lastIndexOf("->") + 2);
//                JSONObject jsonObject = JSONUtil.parseObj(data);
//                jsonObject.set("dataFlowDate", DateUtil.formatDate(DateUtil.date(jsonObject.getLong("dataFlowDate"))));
//            });
////            Thread.sleep(1000L);
//        }
//    }
//}
