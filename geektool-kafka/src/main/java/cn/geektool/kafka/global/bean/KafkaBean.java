package cn.geektool.kafka.global.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * kafka通用配置
 *
 * @author jiangdi
 * @since 0.0.1
 */
@Data
public class KafkaBean {

    @JSONField(name = "bootstrap.servers")
    private String servers;
}
