package cn.geektool.core.builder;

public interface BaseBuilder<T> {
    /**
     * 构建
     *
     * @return 被构建的对象
     */
    T builder();
}