package cn.geektool.es.server.annoation;

/**
 * ES字段注解保留
 *
 * @author jiangdi
 * @since 1.0.0
 */
public @interface ESField {

}
