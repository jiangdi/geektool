package cn.geektool.http.server;

import cn.geektool.core.swing.DesktopUtil;
import cn.geektool.http.HttpUtil;

public class DocServerTest {

	public static void main(String[] args) {
		HttpUtil.createServer(80)
				// 设置默认根目录，
				.setRoot("D:\\workspace\\site\\hutool-site")
				.start();

		DesktopUtil.browse("http://localhost/");
	}
}
