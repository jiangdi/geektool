package cn.geektool.core.io;

import org.junit.Assert;
import org.junit.Test;

import cn.geektool.core.io.file.FileReader;

/**
 * 文件读取测试
 * @author jd
 *
 */
public class FileReaderTest {
	
	// @Test
	public void fileReaderTest(){
		FileReader fileReader = new FileReader("test.properties");
		String result = fileReader.readString();
		Assert.assertNotNull(result);
	}
}
