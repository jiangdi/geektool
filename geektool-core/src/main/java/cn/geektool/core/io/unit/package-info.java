/**
 * 数据单位相关封装，包括DataUnit数据单位和DataSize数据大小
 * 
 * @author jd
 * @since 5.3.10
 */
package cn.geektool.core.io.unit;