/**
 * 剪贴板相关的工具，包括剪贴板监听等
 * 
 * @author jd
 *
 */
package cn.geektool.core.swing.clipboard;