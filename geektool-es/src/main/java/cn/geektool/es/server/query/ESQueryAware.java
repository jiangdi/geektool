package cn.geektool.es.server.query;

import cn.geektool.es.model.query.ESPage;

import java.util.List;

/**
 * 查询类
 */
public interface ESQueryAware {

    <T> List<T> list(Class<T> clz);

    <T> ESPage<T> page(Class<T> clz);

    <T> T single(Class<T> clz);

}
