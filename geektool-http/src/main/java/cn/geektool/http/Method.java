package cn.geektool.http;

/**
 * Http方法枚举
 *
 * @author jd
 */
public enum Method {
	GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, CONNECT, PATCH
}
