/**
 * 提供文本相关操作的封装，还包括Unicode工具UnicodeUtil
 * 
 * @author jd
 *
 */
package cn.geektool.core.text;