package cn.geektool.core.convert.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import cn.geektool.core.util.BooleanUtil;
import cn.geektool.core.convert.AbstractConverter;

/**
 * {@link AtomicBoolean}转换器
 * 
 * @author jd
 * @since 3.0.8
 */
public class AtomicBooleanConverter extends AbstractConverter<AtomicBoolean> {
	private static final long serialVersionUID = 1L;

	@Override
	protected AtomicBoolean convertInternal(Object value) {
		if (value instanceof Boolean) {
			return new AtomicBoolean((Boolean) value);
		}
		final String valueStr = convertToStr(value);
		return new AtomicBoolean(BooleanUtil.toBoolean(valueStr));
	}

}
