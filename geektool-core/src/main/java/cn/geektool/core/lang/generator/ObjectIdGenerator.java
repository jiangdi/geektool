package cn.geektool.core.lang.generator;

import cn.geektool.core.lang.ObjectId;

/**
 * ObjectId生成器
 *
 * @author jd
 * @since 5.4.3
 */
public class ObjectIdGenerator implements Generator<String> {
	@Override
	public String next() {
		return ObjectId.next();
	}
}
