package cn.geektool.kafka.admin.external;

import cn.geektool.core.reflect.BaseReflectInvoke;

import java.util.concurrent.ExecutionException;

/**
 * 消费者接口--可自行实现
 *
 * @author jiangdi
 * @since 0.0.1
 */
public interface IKafkaConsumer<T> extends Runnable {

    /**
     * 回调
     *
     * @param invoke 回调处理的方式
     * @return kafka消费者
     */
    IKafkaConsumer invoke(BaseReflectInvoke invoke);

    /**
     * 执行
     *
     * @param t 执行的参数
     * @throws ExecutionException   执行异常
     * @throws InterruptedException 断开链接异常
     */
    void execute(T t) throws ExecutionException, InterruptedException;
}
