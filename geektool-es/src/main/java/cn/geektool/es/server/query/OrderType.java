package cn.geektool.es.server.query;

/**
 * 支持的排序类型
 *
 * @author jiangdi
 * @since 1.0.0
 */
public enum OrderType {
    ASC, DESC;
}
