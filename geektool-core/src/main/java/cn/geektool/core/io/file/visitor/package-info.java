/**
 * FileVisitor功能性实现，包括递归删除、拷贝等
 * 
 * @author jd
 *
 */
package cn.geektool.core.io.file.visitor;