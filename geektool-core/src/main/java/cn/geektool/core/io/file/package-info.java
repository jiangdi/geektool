/**
 * 对文件读写的封装，包括文件拷贝、文件读取、文件写出、行处理等
 * 
 * @author jd
 *
 */
package cn.geektool.core.io.file;