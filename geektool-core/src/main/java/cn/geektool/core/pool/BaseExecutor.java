package cn.geektool.core.pool;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/**
 * 重构线程池的执行
 *
 * @author jiangdi
 * @since 0.0.1
 */
public interface BaseExecutor extends Executor {

    /**
     * 执行
     *
     * @param command 执行的线程
     * @return 执行结果
     */
    Future<?> submit(Runnable command);
}
