package cn.geektool.core.observer;

/**
 * 观察者类型
 *
 * @author jiangdi
 * @since 0.0.1
 */
public enum ObserverType {

    /**
     * 同步
     */
    SYNC(1),

    /**
     * 异步
     */
    NSYNC(2);

    private final int value;

    public int getValue() {
        return value;
    }

    ObserverType(int value) {
        this.value = value;
    }
}