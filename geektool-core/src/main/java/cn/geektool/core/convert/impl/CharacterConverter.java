package cn.geektool.core.convert.impl;

import cn.geektool.core.util.BooleanUtil;
import cn.geektool.core.convert.AbstractConverter;
import cn.geektool.core.util.StrUtil;

/**
 * 字符转换器
 * 
 * @author jd
 *
 */
public class CharacterConverter extends AbstractConverter<Character> {
	private static final long serialVersionUID = 1L;

	@Override
	protected Character convertInternal(Object value) {
		if (value instanceof Boolean) {
			return BooleanUtil.toCharacter((Boolean) value);
		} else {
			final String valueStr = convertToStr(value);
			if (StrUtil.isNotBlank(valueStr)) {
				return valueStr.charAt(0);
			}
		}
		return null;
	}

}
