package cn.geektool.es.admin.external;

import cn.geektool.es.admin.factory.IESFactory;
import cn.geektool.es.server.ESFactory;

/**
 * es扩展
 *
 * @author jiangdi
 * @since 1.0.0
 */
public interface IESExternal {

    /**
     * 创建
     *
     * @param key
     * @return
     */
    default IESFactory create(String key) {
        return new ESFactory();
    }
}