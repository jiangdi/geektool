package cn.geeltool.poi.word;

import cn.afterturn.easypoi.word.parse.ParseWord07;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.util.List;
import java.util.Map;

/**
 * Word使用模板导出工具类
 *
 * 原作者：https://gitee.com/lemur/easypoi/tree/master
 * @author jiangdi
 *  2021-03-10
 * @version 1.0
 */
public class WordExportUtil {

    private WordExportUtil() {

    }

    /**
     * 解析Word2007版本
     * 
     * @param url
     *            模板地址
     * @param map
     *            解析数据源
     * @return 无返回值
     */
    public static XWPFDocument exportWord07(String url, Map<String, Object> map) throws Exception {
        return new ParseWord07().parseWord(url, map);
    }

    /**
     * 解析Word2007版本
     * 
     * @param document
     *            模板
     * @param map
     *            解析数据源
     */
    public static void exportWord07(XWPFDocument document, Map<String, Object> map) throws Exception {
        new ParseWord07().parseWord(document, map);
    }

    /**
     * 一个模板生成多页
     * @param url url
     * @param list list
     * @return 无返回值
     * @throws Exception 异常
     */
    public static XWPFDocument exportWord07(String url, List<Map<String, Object>> list) throws Exception {
        return new ParseWord07().parseWord(url, list);
    }


}
